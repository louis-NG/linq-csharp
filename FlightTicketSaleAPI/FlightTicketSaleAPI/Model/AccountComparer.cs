﻿using FlightTicketSaleAPI.ADO;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace FlightTicketSaleAPI.Model
{
    public class AccountComparer : IEqualityComparer<Account>
    {
        public bool Equals([AllowNull] Account x, [AllowNull] Account y)
        {
            if (x.Name.Equals(y.Name)) return true;

            return false;
        }   

        public int GetHashCode([DisallowNull] Account obj)
        {
            throw new NotImplementedException();
        }

    }

}
