﻿using FlightTicketSaleAPI.ADO;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace FlightTicketSaleAPI.Model
{
    public class AccountSequenceEqual : IEqualityComparer<Account>
    {
        public bool Equals([AllowNull] Account x, [AllowNull] Account y)
        {
            if (x.Name.Trim().Equals(y.Name.Trim()) && x.Balance == y.Balance) return true;
            return false;
        }

        public int GetHashCode([DisallowNull] Account obj)
        {
            throw new NotImplementedException();
        }
    }
}
