﻿using System;
using System.Collections.Generic;

namespace FlightTicketSaleAPI.ADO
{
    public partial class Author
    {
        public Author()
        {
            Account = new HashSet<Account>();
        }

        public int Id { get; set; }
        public string Authorize { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
