﻿using System;
using System.Collections.Generic;

namespace FlightTicketSaleAPI.ADO
{
    public partial class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public bool? Gender { get; set; }
        public string Address { get; set; }
        public long Balance { get; set; }
        public int IdAuthor { get; set; }

        public virtual Author IdAuthorNavigation { get; set; }
    }
}
