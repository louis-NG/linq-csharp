﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FlightTicketSaleAPI.ADO;
using System.Linq.Expressions;
using System.Collections;
using System.Web.Http.Description;
using FlightTicketSaleAPI.Model;

namespace FlightTicketSaleAPI.Controllers
{
    [Route("api/Accounts")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        public Func<Account, bool> perBalance = u => u.Balance < 500;
        Expression<Func<Account, bool>> predicate = u => u.Balance > 200;
        private readonly ShopSaleContext _context;

        public AccountsController(ShopSaleContext context)
        {
            _context = context;
        }

        // GET: api/Accounts
        [HttpGet]
        public IEnumerable<Account> GetAccountByWhere()
        {
            //  var getAllAccount = from s in _context.Account where s.Balance > 200 select s.Name;
            List<Account> getAllAccount = new List<Account>(_context.Account.Where(predicate).Where(perBalance));
            return getAllAccount;
        }
        [HttpGet("/ofType")]
        public void GetAccountByOfType()
        {
            IList mixedList = new ArrayList();
            mixedList.Add(0);
            mixedList.Add("One");
            mixedList.Add("Two");
            mixedList.Add(3);
            mixedList.Add(new Account() { Id = 1, Address = "56 D1 Street" });

            var getAccountByOfType = mixedList.OfType<string>();
            foreach (var i in getAccountByOfType)
            {
                Console.WriteLine(i);
            }
        }
        [HttpGet,Route("/ASC")]
        [ResponseType(typeof(List<Account>))]
        public ActionResult getAccountOrderByASC()
        {
            List<Account> getAllAccount = new List<Account>(_context.Account.OrderBy(s => s.Balance));
            return Ok(getAllAccount);
        }

        [HttpGet, Route("/DES")]
        [ResponseType(typeof(List<Account>))]
        public ActionResult getAccountOrderByDES()
        {
            List<Account> getAllAccount = new List<Account>(_context.Account.OrderByDescending(s => s.Balance));
            return Ok(getAllAccount);
        }

        [HttpGet, Route("/GroupBy")]
        //[ResponseType(typeof(Dictionary<int,List<Account>>))]
        public ActionResult getAccountGroupByAuthor()
        {
            var getAccount = _context.Account.GroupBy(s => s.Name);
            return Ok();
        }

        [HttpGet, Route("/Lookup")]
        //[ResponseType(typeof(Dictionary<int,List<Account>>))]
        public ActionResult getAccountLookupAuthor()
        {
            var getAccount = _context.Account.ToLookup(s => s.IdAuthor);
            return Ok();
        }
        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Account>> GetAccount(int id)
        {
            var account = await _context.Account.FindAsync(id);

            if (account == null)
            {
                return NotFound();
            }

            return account;
        }

        [HttpGet, Route("/Join")]
        public ActionResult GetAllAccountJoinAuthor()
        {
            var accounts = _context.Account.Join(_context.Author, account => account.IdAuthor, author => author.Id, (account, author) => new
            {
                Name = account.Name,
                Birthday = account.Birthday,
                Address = account.Address,
                Author = author.Authorize
            }) ;
            return Ok(accounts);
        }
        [HttpGet, Route("/GroupJoin")]
        public ActionResult GetAllAccountGroupJoinAuthor()
        {
            var accounts = _context.Author.GroupJoin(_context.Account, author => author.Id, account => account.IdAuthor, (author, accounts) => new
            {
                Author = author.Authorize,
                GroupAccount = accounts
            });
            return Ok(accounts);
        }

        [HttpGet, Route("/Select")]
        public ActionResult GetAllAccountBySelect()
        {
            var accounts = _context.Account.Select(s => new { Name = s.Name, BirthDay = s.Birthday });
            return Ok(accounts);
        }

        [HttpGet, Route("/All")]
        public ActionResult CheckAccountByAll()
        {
            bool check = _context.Account.All(s => s.Balance > 300);
            return Ok(check);
        }

        [HttpGet, Route("/Any")]
        public ActionResult CheckAccountByAny()
        {
            bool check = _context.Account.Any(s => s.Balance > 300);
            return Ok(check);
        }

        [HttpGet, Route("/Contain")]
        public ActionResult CheckAccountByContain()
        {
            Account account = new Account() { Name = "Mark", Balance = 200, Address ="", Birthday=DateTime.Today,Gender=true,Id=10,IdAuthor=1 };
            bool check = _context.Account.Contains(account, new AccountComparer());
            return Ok(check);
        }

        [HttpGet, Route("/Aggregate")]
        public ActionResult GetAccountByAggregate()
        {
            IList<String> strList = new List<String>() { "One", "Two", "Three", "Four", "Five" };

            var commaSeperatedString = strList.Aggregate((s1, s2) => s1 + ", " + s2);

            return Ok(commaSeperatedString);
        }
        [HttpGet, Route("/Aggregate1")]
        public ActionResult GetAccountByAggregate1()
        {
            var commaSeperatedString = _context.Account.Aggregate<Account,string>("Account Names: ",  // seed value
                                        (str, s) => str +"," +s.Name
                                        );

            return Ok(commaSeperatedString);
        }

        [HttpGet, Route("/Average")]
        public ActionResult getAverageBalance()
        {
            var average = _context.Account.Average(s => s.Balance);
            return Ok(average);
        }

        [HttpGet,Route("/Count")]
        public ActionResult getCountBalance()
        {
            var countBalance = _context.Account.Count(s => s.Balance > 200);
            return Ok(countBalance);
        }

        [HttpGet,Route("/Max")]
        public ActionResult getMaxBalance()
        {
            var Max = _context.Account.Max(s => s.Balance);
            return Ok(Max);
        }
        [HttpGet,Route("/Sum")]
        public ActionResult getSumAccount()
        {
            var Sum = _context.Account.Sum(s => s.Balance);
            return Ok(Sum);
        }

        [HttpGet,Route("/SequenceEqual")]
        public ActionResult comparerAccount()
        {
            Account account =  _context.Account.FirstOrDefault() ;
           List<Account> account1 = new List<Account>() { account };
            Account _account = _context.Account.AsEnumerable().Last();
            List<Account> account2 = new List<Account>() { _account };

            var check = account1.SequenceEqual(account2,new AccountSequenceEqual());
            return Ok(check);
        }
        [HttpGet, Route("/getAll")]
        public ActionResult getAll()
        {
            var getall = _context.Account.Select(s=>s);
            return Ok(getall);
        }

        [HttpGet, Route("/Concat")]
        public ActionResult ConcatString()
        {
            List<string> string1 = new List<string>() { "One", "Two", "Three" };
            List<string> string2 = new List<string>() { "Four", "Five"};

            var string3 = string1.Concat(string2);
            return Ok(string3);
        }
        [HttpGet, Route("/DefaultIfEmpty")]
        public  ActionResult CollectionStringEmpty()
        {
            List<string> string1 = new List<string>();
            List<string> string2 = new List<string>();

            var newList1 = string1.DefaultIfEmpty();
            var newList2 = string2.DefaultIfEmpty("None");

            var result ="List1 :" + newList1.ElementAt(0)+ "; List2:" + newList2.ElementAt(0);
            return Ok(result);
        }

        // PUT: api/Accounts/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details s ee https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccount(int id, Account account)
        {
            if (id != account.Id)
            {
                return BadRequest();
            }

            _context.Entry(account).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Accounts
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Account>> PostAccount(Account account)
        {
            _context.Account.Add(account);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccount", new { id = account.Id }, account);
        }

        // DELETE: api/Accounts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Account>> DeleteAccount(int id)
        {
            var account = await _context.Account.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Account.Remove(account);
            await _context.SaveChangesAsync();

            return account;
        }

        private bool AccountExists(int id)
        {
            return _context.Account.Any(e => e.Id == id);
        }

    }
}
